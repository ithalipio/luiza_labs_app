from flask import jsonify, request

import facebook
from app.main import app
from app.util.errorhandler import handle_error


def init_routes():
    @app.route("/person/", methods=['GET'])
    def get_person():
        limit = request.args.get('limit')
        items = facebook.get_user_from_database(limit)
        if items is not None and len(items) > 0:
            return jsonify([i.serialize for i in items])
        else:
            handle_error(404)

    @app.route("/person/", methods=['POST'])
    def create_person():
        facebook_id = request.form['facebookId']
        result = facebook.create_user(facebook_id)
        if result is not None:
            return '', 201
        handle_error(400)

    @app.route("/person/<user_id>/", methods=['DELETE'])
    def delete_user(user_id):
        result = facebook.delete_user(user_id)
        if result is not None:
            return '', 204
        handle_error(404)


