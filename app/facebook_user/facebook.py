import json

import requests
from sqlalchemy import exc

from app.main import app
from app.util.database import db
from models import FacebookUser

facebook_base_url = app.config['FACEBOOK_BASE_URL']
access_token = app.config['FACEBOOK_ACCESS_TOKEN']


def get_user_from_facebook(facebook_id):
    url = facebook_base_url + facebook_id
    req_params = {'access_token': access_token, 'fields': 'id, name, gender'}
    app.logger.info('[Request - Facebook API]. URL: %s', url)
    res = requests.get(url, params=req_params)
    app.logger.info('[Response - Facebook API]. STATUS CODE: %s, RESPONSE CONTENT: %s', res.status_code, res._content)
    if res.status_code == 200:
        return json.loads(res.text)
    return None


def get_user_from_database(limit):
    if limit is not None:
        limit = int(limit)
        app.logger.info('[DATABASE] - Executing select')
        return FacebookUser.query.limit(limit).all()
    else:
        app.logger.info('[DATABASE] - Executing select')
        return FacebookUser.query.all()


def create_user(facebook_id):
    facebook_response = get_user_from_facebook(facebook_id)
    if facebook_response:
        formated_user = format_user(facebook_response)
        new_user = FacebookUser(formated_user['username'], formated_user['id'], formated_user['name'], formated_user['gender'])
        try:
            app.logger.info('[DATABASE] - Executing insert')
            db.session.add(new_user)
            db.session.commit()
            return new_user
        except exc.SQLAlchemyError as e:
            app.logger.error('[DATABASE ERROR] %s', e)
            db.session.rollback()
        finally:
            db.session.flush()
    return None


def delete_user(user_id):
    user = FacebookUser.query.filter_by(facebook_id=user_id).first()
    if user is not None:
        try:
            app.logger.info('[DATABASE] - Executing delete')
            db.session.delete(user)
            db.session.commit()
            return user
        except exc.SQLAlchemyError as e:
            app.logger.error('[DATABASE ERROR] %s', e)
            db.session.rollback()
        finally:
            db.session.flush()
    return None


def format_user(user):
    formated_user = {}
    if 'username' in user:
        formated_user['username'] = user['username']
    else:
        formated_user['username'] = None

    if 'id' in user:
        formated_user['id'] = user['id']
    else:
        formated_user['id'] = None

    if 'name' in user:
        formated_user['name'] = user['name']
    else:
        formated_user['name'] = None

    if 'genre' in user:
        formated_user['gender'] = user['gender']
    else:
        formated_user['gender'] = None

    return formated_user


