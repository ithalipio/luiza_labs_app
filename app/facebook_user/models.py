from sqlalchemy.dialects.mysql import BIGINT

from app.util.database import db


class FacebookUser(db.Model):

    __tablename__ = 'facebook_users'
    __table_args__ = {'extend_existing': True}

    username = db.Column(db.String(200), unique=True, nullable=True)
    facebook_id = db.Column(BIGINT(100), primary_key=True, unique=True, nullable=False)
    name = db.Column(db.String(200), nullable=False)
    gender = db.Column(db.String(6), nullable=True)

    def __init__(self, username=None, facebook_id=None, name=None, gender=None):
        self.username = username
        self.facebook_id = facebook_id
        self.name = name
        self.gender = gender

    def __repr__(self):
        return '<User %r>' % self.name

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'username': self.username,
            'facebookId': self.facebook_id,
            'name': self.name,
            'gender': self.gender
        }
