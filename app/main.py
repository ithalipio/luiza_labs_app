from flask import Flask, jsonify
from werkzeug.exceptions import HTTPException

app = Flask(__name__)

"""Load app config from file"""
app.config.from_pyfile("../config.py")


@app.errorhandler(Exception)
def handle_global_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(error=str(e)), code


@app.errorhandler(405)
def handle_405(e):
    code = 405
    return jsonify(error=str(e)), code


@app.errorhandler(404)
def handle_404(e):
    code = 404
    return jsonify(error=str(e)), code
