import unittest
from app.main import app
from app.util.database import db
import ast


class FacebookUserTestCase(unittest.TestCase):

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = app
        db.engine.execute(
            "CREATE DATABASE IF NOT EXISTS TEST")
        db.engine.execute(
            "USE TEST")
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/TEST'
        self.app.run
        self.client = self.app.test_client

        # binds the app to the current context
        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_create_user(self):
        res = self.client().post('/person/', data=dict(facebookId=1234))
        self.assertEqual(res.status_code, 201)
        self.assertIn('', str(res.data))

    def test_get_user(self):
        self.client().post('/person/', data=dict(facebookId=1234))
        res = self.client().get('/person/')
        self.assertEqual(res.status_code, 200)

    def test_get_user_with_limit(self):
        db.engine.execute(
            "INSERT INTO facebook_users (username, facebook_id, name, gender) VALUES ('test', 10, 'test', 'test')")
        db.engine.execute(
            "INSERT INTO facebook_users (username, facebook_id, name, gender) VALUES ('test2', 20, 'test', 'test')")
        db.engine.execute(
            "INSERT INTO facebook_users (username, facebook_id, name, gender) VALUES ('test3', 30, 'test', 'test')")
        res = self.client().get('/person/?limit=2')
        self.assertEqual(res.status_code, 200)
        res_list = ast.literal_eval(res.data)
        self.assertLessEqual(len(res_list), 2)

    def test_delete_user(self):
        self.client().post('/person/', data=dict(facebookId=1234))
        res = self.client().delete('/person/1234/')
        self.assertEqual(res.status_code, 204)
        get_res = self.client().get('/person/')
        self.assertEqual(get_res.status_code, 404)

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()
            db.engine.execute("DROP DATABASE TEST")


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
