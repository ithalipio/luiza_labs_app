from flask_migrate import Migrate

from facebook_user.routes import init_routes
from util.database import db
from main import app
from util import logger

"""Inititalize app routes"""
init_routes()

"""Initialize app logger"""
app.logger.addHandler(logger.fileHandler)
app.logger.addHandler(logger.streamHandler)

"""Set up migration"""
migrate = Migrate(app, db)

"""Necessary import for migrations work"""
from app.facebook_user import models
