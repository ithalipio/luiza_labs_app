from flask import make_response, abort, jsonify
from app.main import app

APP_ERRORS_DICT = {
    '404': 'Resource Not Found',
    '400': 'Bad Request'
}


def handle_error(status_code):
    app.logger.error('[Exception] - %s', APP_ERRORS_DICT[str(status_code)])
    return abort(make_response(jsonify(error=APP_ERRORS_DICT[str(status_code)]), status_code))
