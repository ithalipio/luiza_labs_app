import logging
from logging.handlers import RotatingFileHandler


logFormatStr = '[%(asctime)s] (%(pathname)s:%(lineno)d) %(levelname)s - %(message)s'
logging.basicConfig(format=logFormatStr, filename="logs/stream.log", level=logging.DEBUG)
formatter = logging.Formatter(logFormatStr, '%m-%d %H:%M:%S')
fileHandler = RotatingFileHandler('logs/luiza_labs.log', maxBytes=1024 * 1024 * 100, backupCount=20)
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(formatter)
streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.DEBUG)
streamHandler.setFormatter(formatter)
#
# log_format_str = '[%(asctime)s] (%(pathname)s:%(lineno)d) %(levelname)s - %(message)s'
# logging.basicConfig(format=log_format_str, filename="logs/luiza_labs.log", level=logging.DEBUG)
# # formatter = logging.Formatter(log_format_str, '%m-%d %H:%M:%S')
# log_handler = RotatingFileHandler('logs/luiza_labs.log', maxBytes=100000, backupCount=3)
# log_handler.setFormatter(log_format_str)
# log_handler.setLevel(logging.DEBUG)

# formatter = logging.Formatter("[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
# handler = logging.handlers.RotatingFileHandler(
#     'logs/luiza_labs.log',
#     maxBytes=1024 * 1024 * 100,
#     backupCount=20
#     )
#
# handler.setLevel(logging.DEBUG)
# handler.setFormatter(formatter)
#
# log = logging.getLogger('werkzeug')
# log.setLevel(logging.DEBUG)
# log.addHandler(handler)
