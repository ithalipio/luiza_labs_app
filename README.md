# Luiza Labs Backend Challenge #

### Requisitos: ###
* Python v2.7
* pip v9.0.1
* Mysql

### Setup: ###
Primeiro, é necessário clonar o repositório do projeto, instalar as dependências e criar a estrutura do Banco de Dados. No terminal, digite os seguintes comandos:
* `$ git clone https://gitlab.com/ithalipio/luiza_labs_app.git`
* `$ cd luiza_labs_app`
* `$ pip install -r requirements.txt`
* `$ export FLASK_APP=run.py`

### Configuração: ###
Para executar o projeto, é necessário possuir as variáveis de ambiente listadas abaixo ou editar o arquivo `config.py` e definir os valores.
* `DEBUG` - Flag que indica se o projeto irá executar em modo Debug
    * Valor default: True
* `SQLALCHEMY_DATABASE_URI` - String de conexão com o Banco de Dados MYSQL.
    * Valor default: mysql+pymysql://root:root@localhost/FACEBOOK
* `SQLALCHEMY_ECHO` - Flag que indica se a aplicação irá logar as queries executadas.
    * Valor default: True
* `FACEBOOK_BASE_URL` - URL da Graph API do Facebook
    * Valor default: https://graph.facebook.com/
* `FACEBOOK_ACCESS_TOKEN` - Token de acesso a Graph API do Facebook. (https://developers.facebook.com/docs/facebook-login/access-tokens/?locale=pt_BR)


### Migrations: ###
Para criar a estrutura do Banco de Dados, execute os seguintes passos:
* No Mysql, crie um banco de dados de nome FACEBOOK
   * `CREATE DATABASE FACEBOOK;`
* Execute o comando para criar o diretório migrations:
    * `$ flask db init`
* Agora, execute o comando para criar a estrutura da base:
    * `$ flask db migrate`
* Por fim, execute o comando para atualizar as informações sobre a estrutura:
    * `$ flask db upgrade`


No site do projeto, estão listados todas as especificações e seu funcionamento - https://flask-migrate.readthedocs.io/en/latest/

### Execução: ###
Após as configurações, no diretório do projeto, execute o seguinte comando para startar a aplicação:
* `$ flask run`

A aplicação estará disponível em http://localhost:5000

### Testes: ###
Para executar os testes, execute o seguinte comando:
* `$ python -m unittest discover -s {PATH_WHERE_PROJECT_IS}/luiza_labs_app/app/test -p test_facebook.py`

No comando acima, lembre-se de substituir o {PATH_WHERE_PROJECT_IS} com o path do diretório em que o projeto está.

### Logs: ###
O arquivo de Log está localizado no path `logs/luiza_labs.log`

### Considerações ###
* A API do Facebook, em sua versão mais atual, não disponibiliza todas as informações solicitadas. O atributo "username" foi descontinuado, por isso será salvo como nulo na base. O atributo "gender" só está disponível com token de cliente quando se solicita as informações do próprio cliente.

### Project Stack: ###
O projeto foi desenvolvido utilizando Flask, SQLAlchemy como ORM, Flask-Migrate para database migration e Mysql como banco de dados.

http://flask.pocoo.org/

http://flask-sqlalchemy.pocoo.org/2.3/

https://flask-migrate.readthedocs.io/en/latest/

https://www.mysql.com/

