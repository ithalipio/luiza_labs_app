"""Configuration file"""

import os

DEBUG = os.getenv('DEBUG', True)
SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'mysql+pymysql://root:root@localhost/FACEBOOK')
SQLALCHEMY_ECHO = os.getenv('SQLALCHEMY_ECHO', True)
SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS', False)
FACEBOOK_BASE_URL = os.getenv('FACEBOOK_BASE_URL', 'https://graph.facebook.com/')
FACEBOOK_ACCESS_TOKEN = os.getenv('FACEBOOK_ACCESS_TOKEN')



